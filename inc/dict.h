#pragma once

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "utils.h"

typedef struct {
    char **keys;
    void **values;
    ssize_t len;
    ssize_t cap;
} dict_t;

typedef dict_t *dict;

dict create_dict();

ssize_t dict_key_index(dict d, const char *key);

void *dict_get(dict, const char *key);

void dict_set(dict, const char *key, void *value);

void dict_remove(dict, const char *key, cleaner_t cleaner);

void dict_destroy(dict, cleaner_t cleaner);
