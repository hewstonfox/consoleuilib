#pragma once

#include <time.h>

#include "types.h"

#include "utils.h"
#include "dict.h"
#include "queue.h"
#include "set.h"

typedef void (*evt_handler)(context *);


typedef struct evt_handlers_s *event_handlers_set;
struct evt_handlers_s {
    evt_handler *handlers;
    ssize_t len;
    ssize_t cap;
};

typedef struct event_queue_item_s {
    context *ctx;
    evt_handler callback;
} event_queue_item;

void init_events();

void destroy_events();

event_handlers_set create_event_handlers_set();

native_event *create_native_event();

context *create_event_context(native_event *e, cui_window global_ctx);

void destroy_event_context(context *);

void event_stop_propagation(native_event *e);

void destroy_native_event(native_event *e);

void destroy_event_handler_set(event_handlers_set ehs);

void add_event_handler(event_handlers_set ehs, evt_handler cb);

void remove_event_handler(event_handlers_set ehs, evt_handler cb);

void call_event_emitter(evt_handler cb, native_event *e, cui_window global_ctx, document *emd);

void resolve_ehs(event_handlers_set ehs, native_event *e, cui_window global_ctx, document *emd);

native_event *generate_native_event(cui_window _ctx);

void dispatch_event(native_event *e);

void process_events(cui_window _ctx);

native_event *create_event_of_type(document *target, const char *type);

void resolve_events(cui_window global_ctx);
