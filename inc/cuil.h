#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <X11/Xlib.h>
#include <unistd.h>
#include <time.h>
#include <locale.h>
#include <wchar.h>
#include <wctype.h>
#include <pthread.h>
#include <termios.h>
#include <signal.h>

#include "utils.h"
#include "types.h"
#include "dict.h"
#include "queue.h"
#include "set.h"
#include "events.h"
#include "dom.h"
#include "default_event_handlers.h"

void init_context(int miny, int minx, uint16_t flags, any store);

void destroy_context();

#define __INIT if (!_ctx) init_context(0, 0, CUIL_DEFAULT, NULL);

#define __DESTROY if (_ctx) destroy_context();

cui_window get_context();

void start_event_loop(evt_handler frame_callback, void *(*subprocess)(context *));

void stop_event_loop();

void add_event_listener(document *target, const char *event, evt_handler cb);

void remove_event_listener(document *target, const char *event, evt_handler cb);


document *document_create_div(int maxy, int maxx, int begy, int begx);

document *document_create_input(int maxy, int maxx, int begy, int begx);

void document_set_text(document *doc, const wchar_t *text);

void change_focus_tab(document *root, int reverse);

void draw_frame(document *doc, int force);

void focus(document *doc);

void blur(document *doc);


void set_cursor(cui_window app);
