#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <wchar.h>
#include <wctype.h>

#ifndef NCURSES_WIDECHAR
#define NCURSES_WIDECHAR   1
#endif

#if __linux__

#include <curses.h>

#elif __APPLE__

#include <ncurses.h>

#endif

typedef void (*cleaner_t)(void *);


void null_free(void **);

char *clonestr(const char *src);

wchar_t *wclonestr(const wchar_t *src);

wchar_t *newwstr(size_t len);

wchar_t char2wchar(char src);

wchar_t *chars2wchars(const char *src);

void remove_by_idx(void ***arr, size_t size, size_t idx, cleaner_t cleaner);

bool is_printable(unsigned long c);

bool is_caret_move(unsigned long c);

bool is_backspace(unsigned long c);

bool is_remove_char(unsigned long c);

void win_clean(WINDOW *win);

