#pragma once

#include "dom.h"
#include "wchar.h"
#include "cuil.h"

void EMBEDDED_tab_focus_switch_handler(context *ctx);


void EMBEDDED_input_input_handler(context *ctx);

void EMBEDDED_input_focus_handler(context *ctx);

void EMBEDDED_input_blur_handler(context *ctx);
