#pragma once

#include <time.h>

#if __linux__

#include <curses.h>

#elif __APPLE__
#include <ncurses.h>
#endif

#include "dict.h"
#include "types.h"
#include "events.h"

document *create_document();

void destroy_document(document *doc);

void document_append_child(document *parent, document *child);

document *document_get_root(document *doc);

document *get_focused_document(document *target);

void document_unfocus(document *doc);

void document_focus(document *doc);

document *get_next_focus(document *doc, int ignore_children);

document *get_prev_focus(document *doc, int ignore_parent);

void document_request_update(document *doc);

void document_request_hard_update(document *doc);
