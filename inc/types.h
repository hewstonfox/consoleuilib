#pragma once

#include "dict.h"

#ifndef NCURSES_WIDECHAR
#define NCURSES_WIDECHAR   1
#endif

#if __linux__

#include <curses.h>
#include <malloc.h>

#define malloc_size malloc_usable_size
#elif __APPLE__

#include <ncurses.h>
#include <malloc/malloc.h>

#endif

#define COMPONENT_TYPE_DIV 1
#define COMPONENT_TYPE_INPUT 2
#define COMPONENT_TYPE_OTHER 3

typedef struct dom_s document;
typedef struct cui_window_s *cui_window;
typedef struct native_event_s native_event;
typedef void *any;

typedef struct position_s {
    int x, y;
} position;

typedef struct hidden_attributes_s {
    WINDOW *extra_win;
    wchar_t *value;
    position cur_pos;
    short cursor;
} hidden_attributes;

struct dom_s {
    char *name;
    int type;
    long int key;
    int show;
    dict styles;
    dict state;
    document **children;
    wchar_t *text;
    size_t ch_len;
    document *parent;
    WINDOW *win;
    int should_update;
    int should_hard_update;
    dict event_handlers;
    cui_window ctx;
    hidden_attributes _hidden_attr;
};

struct cui_window_s {
    int max_x;
    int max_y;
    uint16_t flags;
    int is_running;
    document *dom;
    document *focused;
    any store;
};

struct native_event_s {
    char *type;
    int is_stopped;
    int ret;
    unsigned int code;
    unsigned int *codes;
    int cd_len;
    MEVENT *mouse_event;
    document *target;
};


typedef struct context_s {
    cui_window app;
    native_event *event;
    document *emitter;
    any store;
} context;

#define IS_FOCUSED(x) ((x)->ctx->focused == (x))

#define CUIL_DEFAULT 0
//#define CUIL_FULLSCREEN (1 << 0)
//#define CUIL_EXTERNAL_TERM (1 << 1)
#define CUIL_KBRAW (1 << 2)

#define EVENT_KEYBOARD_ONLY "EVENT_KEYBOARD_ONLY"
#define EVENT_MOUSE_ONLY "EVENT_MOUSE_ONLY"
#define EVENT_FOCUS "EVENT_FOCUS"
#define EVENT_BLUR "EVENT_BLUR"
#define EVENT_ALL "EVENT_ALL"

#define EVT_TIMEOUT 100

typedef struct border_preset_t {
    wchar_t lt;
    wchar_t rt;
    wchar_t lb;
    wchar_t rb;
    wchar_t l;
    wchar_t r;
    wchar_t t;
    wchar_t b;
} border_preset;

position get_cursor_position(document *doc);

#define BORDER_NONE 0
#define BORDER_BOLD 1
#define BORDER_BOLD_CORNERS 2
#define BORDER_THIN 3
#define BORDER_THIN_CORNERS 4
