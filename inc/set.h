#pragma once

#include <stdlib.h>
#include <sys/types.h>

#include "utils.h"

typedef struct set_s {
    ssize_t len;
    ssize_t cap;
    void **data;
} set;

set *create_set();

void set_push(set *, void *data);

bool set_includes(set *, void *data);

void *set_pop(set *);

void set_remove(set *, void *data);

void destroy_set(set *, cleaner_t cleaner);
