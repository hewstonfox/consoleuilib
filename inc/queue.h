#pragma once

#include <stdlib.h>
#include <sys/types.h>

#include "utils.h"

typedef struct queue_s {
    ssize_t len;
    ssize_t cap;
    void **data;
} queue;

queue *create_queue();

void queue_push(queue *, void *data);

bool queue_includes(queue *q, void *data);

void *queue_pop(queue *);

void destroy_queue(queue *, cleaner_t cleaner);
