#include "cuil.h"

static volatile cui_window _ctx = NULL;

static struct winsize start_size;

static volatile int _resized = 0;

void win_resize_handler(int sig) {
    sig++;
    _resized = 1;
}

void check_resolution(int miny, int minx) {
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    start_size = w;
    if (w.ws_row >= miny && w.ws_col >= minx) return;
    signal(SIGWINCH, win_resize_handler);
    printf("\033[8;%d;%dt", miny > w.ws_row ? miny : w.ws_row, minx > w.ws_col ? minx : w.ws_col);
    fflush(stdout);
    tcdrain(STDOUT_FILENO);
    long t_start = time(NULL);
    while (!_resized && (time(NULL) - 1 <= t_start));
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    if (w.ws_row < miny || w.ws_col < minx) {
        fprintf(stderr, "Unable to resize console.\n");
        fprintf(stderr, "Min resolution should be %dx%d characters.\n", miny, minx);
        fprintf(stderr, "Nearest available one is %dx%d characters.\n", w.ws_row, w.ws_col);
        fprintf(stderr, "\033[8;%d;%dt", start_size.ws_row, start_size.ws_col);
        exit(2);
    }
}

void init_context(int miny, int minx, uint16_t flags, any store) {
    setlocale(LC_ALL, "");
    check_resolution(miny, minx);
    _ctx = (cui_window) malloc(sizeof(struct cui_window_s));
    _ctx->store = store;
    _ctx->dom = create_document();
    _ctx->dom->ctx = _ctx;
    _ctx->dom->name = clonestr("$$root");
    _ctx->dom->win = initscr();
    init_events();
    add_event_listener(_ctx->dom, EVENT_KEYBOARD_ONLY, &EMBEDDED_tab_focus_switch_handler);
    _ctx->focused = _ctx->dom;
    _ctx->flags = flags;
    getmaxyx(_ctx->dom->win, _ctx->max_y, _ctx->max_x);
    if (has_colors() == TRUE) start_color();
    if (flags & CUIL_KBRAW) raw();
    else cbreak();
    noecho();
    keypad(stdscr, TRUE);
    scrollok(stdscr, TRUE);
    timeout(EVT_TIMEOUT);
    mousemask(ALL_MOUSE_EVENTS, NULL);
    curs_set(0);
    clear();
}

cui_window get_context() {
    __INIT
    return _ctx;
}

void destroy_context() {
    if (!_ctx) return;
    destroy_document(_ctx->dom);
    destroy_events();
    endwin();
    printf("\033[8;%d;%dt", start_size.ws_row, start_size.ws_col);
    free(_ctx);
}

void start_event_loop(evt_handler frame_callback, void *(*subprocess)(context *)) {
    __INIT
    _ctx->is_running = 1;
    context ctx = {_ctx, NULL, NULL, _ctx->store};

    if (subprocess) {
        pthread_t tid;
        pthread_create(&tid, NULL, (void *(*)(void *)) subprocess, &ctx);
    }
    while (_ctx->is_running) {
        curs_set(0);
        draw_frame(_ctx->dom, 0);
        set_cursor(_ctx);
        process_events(_ctx);
        if (frame_callback) frame_callback(&ctx);
    }
}

void stop_event_loop() {
    if (!_ctx) return;
    _ctx->is_running = 0;
}

void add_event_listener(document *target, const char *event, evt_handler cb) {
    dict d = target->event_handlers;
    if (dict_key_index(d, event) == -1)
        dict_set(d, event, create_event_handlers_set());
    add_event_handler((event_handlers_set) dict_get(d, event), cb);
}

void remove_event_listener(document *target, const char *event, evt_handler cb) {
    dict d = target->event_handlers;
    if (dict_key_index(d, event) == -1) return;
    remove_event_handler((event_handlers_set) dict_get(d, event), cb);
}
