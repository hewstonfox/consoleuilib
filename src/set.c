#include "set.h"

set *create_set() {
    set *s = (set *) malloc(sizeof(set));
    s->cap = 10;
    s->len = 0;
    s->data = (void **) malloc(sizeof(void *) * s->cap);
    return s;
}

void set_push(set *s, void *data) {
    if (set_includes(s, data)) return;
    s->data[s->len++] = data;
    if (s->len == s->cap) {
        s->cap = s->cap * 2;
        void **new_data = (void **) malloc(sizeof(void *) * s->cap);
        for (int i = 0; i < s->len; ++i) new_data[i] = s->data[i];
        free(s->data);
        s->data = new_data;
    }
}

bool set_includes(set *s, void *data) {
    for (int i = 0; i < s->len; ++i)
        if (s->data[i] == data) return true;
    return false;
}

void *set_pop(set *s) {
    if (!s->len) return NULL;
    void *data = s->data[0];
    for (int i = 1; i < s->len; ++i)
        s->data[i - 1] = s->data[i];
    s->len--;
    return data;
}

void set_remove(set *s, void *data) {
    ssize_t new_len = s->len;
    for (int gap = 0, i = 0; i < s->len; i++, gap++) {
        if (s->data[i] == data) {
            gap++;
            new_len--;
        }
        if (i == gap) continue;
        s->data[i] = gap < s->len ? s->data[gap] : NULL;
    }
    s->len = new_len;
}

void destroy_set(set *s, cleaner_t cleaner) {
    if (cleaner)
        for (int i = 0; i < s->len; ++i)
            cleaner(s->data[i]);
    free(s->data);
    free(s);
}
