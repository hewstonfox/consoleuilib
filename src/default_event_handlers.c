#include "default_event_handlers.h"

void EMBEDDED_tab_focus_switch_handler(context *ctx) {
    if (ctx->event->code == '\t') change_focus_tab(ctx->event->target, 0);
    else if (ctx->event->code == 353) change_focus_tab(ctx->event->target, 1);
}

static void remove_backward(context *ctx) {
    native_event *e = ctx->event;
    unsigned int c = e->code;
    document *target = e->target;
    if (!is_backspace(c)) return;
    wchar_t *new_val, *cur_val = target->_hidden_attr.value;
    size_t new_len, len = cur_val ? wcslen(cur_val) : 0;
    int max_x = getmaxx(e->target->win);
    int
            x = target->_hidden_attr.cur_pos.x,
            y = target->_hidden_attr.cur_pos.y;
    unsigned int cur_pos = max_x * y + x;
    int deletions = 0;
    if (!cur_val || !len) return;
    if (c == 8) {
        while (cur_pos - deletions && cur_val[cur_pos - deletions - 1] != L' ') deletions++;
        while (cur_pos - deletions && cur_val[cur_pos - deletions - 1] == L' ') deletions++;
    } else if (cur_pos - 1 < len) {
        deletions = 1;
    } else return;
    new_len = len - deletions;
    if (!new_len) new_val = wclonestr(L"");
    else {
        new_val = newwstr(new_len);
        wcsncat(new_val, cur_val, cur_pos - deletions);
        wcsncat(new_val, cur_val + cur_pos, len - cur_pos);
    }
    while (deletions--)
        if (x > 0) x--;
        else if (y) {
            x = max_x - 1;
            y--;
        }
    target->_hidden_attr.cur_pos.x = x;
    target->_hidden_attr.cur_pos.y = y;
    free(cur_val);
    target->_hidden_attr.value = new_val;
}

static void add_char(context *ctx) {
    native_event *e = ctx->event;
    unsigned int c = e->code;
    document *target = e->target;
    if (!is_printable(c)) return;
    wchar_t *new_val, *cur_val = target->_hidden_attr.value;
    size_t new_len, len = cur_val ? wcslen(cur_val) : 0;
    int max_x = getmaxx(e->target->win);
    int
            x = target->_hidden_attr.cur_pos.x,
            y = target->_hidden_attr.cur_pos.y;
    unsigned int cur_pos = max_x * y + x;
    new_len = len + 1;
    new_val = newwstr(new_len);
    wcsncat(new_val, cur_val, cur_pos);
    new_val[cur_pos] = (int) c;
    wcsncat(new_val, cur_val + cur_pos, len - cur_pos);
    if (x < max_x - 1) x++;
    else {
        x = 0;
        y++;
    }
    target->_hidden_attr.cur_pos.x = x;
    target->_hidden_attr.cur_pos.y = y;
    if (cur_val) free(cur_val);
    target->_hidden_attr.value = new_val;
}

static void move_caret(context *ctx) {
    native_event *e = ctx->event;
    unsigned int c = e->code;
    document *target = e->target;
    if (!is_caret_move(c)) return;
    wchar_t *cur_val = target->_hidden_attr.value;
    size_t len = cur_val ? wcslen(cur_val) : 0;
    int max_x = getmaxx(e->target->win);
    int
            x = target->_hidden_attr.cur_pos.x,
            y = target->_hidden_attr.cur_pos.y;
    unsigned int cur_pos = max_x * y + x;
    switch (c) {
        case KEY_LEFT: {
            if (x > 0) x--;
            else if (y) {
                x = max_x - 1;
                y--;
            }
            break;
        }
        case KEY_RIGHT: {
            if (cur_pos == len) break;
            if (x < max_x - 1) x++;
            else {
                x = 0;
                y++;
            }
            break;
        }
        case KEY_DOWN: {
            int maxy = (len / max_x + (len % max_x) != 0);
            if (y < maxy) {
                y++;
                if (y == maxy &&
                    (unsigned long) x > len % max_x)
                    x = (int) len % max_x;
            }
            break;
        }
        case KEY_UP: {
            if (y > 0) y--;
            break;
        }
        default:
            break;
    }
    target->_hidden_attr.cur_pos.x = x;
    target->_hidden_attr.cur_pos.y = y;
}

void EMBEDDED_input_input_handler(context *ctx) {
    unsigned int c = ctx->event->code;
    if (is_backspace(c)) remove_backward(ctx);
    else if (is_caret_move(c)) move_caret(ctx);
    else if (is_printable(c)) add_char(ctx);
    else return;
    event_stop_propagation(ctx->event);
    document_request_update(ctx->event->target);
}

void EMBEDDED_input_focus_handler(context *ctx) {
    ctx->event->target->_hidden_attr.cursor = 1;
}

void EMBEDDED_input_blur_handler(context *ctx) {
    ctx->event->target->_hidden_attr.cursor = 0;
}
