#include "cuil.h"

void change_focus_tab(document *root, int reverse) {
    document *curr_focus = root->ctx->focused;
    if (!curr_focus) focus(root);
    else focus(reverse ? get_prev_focus(curr_focus, 0) : get_next_focus(curr_focus, 0));
    document_get_root(root)->should_update = 1;
}

document *document_create_div(int maxy, int maxx, int begy, int begx) {
    document *div = create_document();
    div->type = COMPONENT_TYPE_DIV;
    div->win = newwin(maxy, maxx, begy, begx);
    return div;
}

document *document_create_input(int maxy, int maxx, int begy, int begx) {
    document *input = create_document();
    input->type = COMPONENT_TYPE_INPUT;
    input->win = newwin(maxy, maxx, begy, begx);
    input->_hidden_attr.value = wclonestr(L"");
    input->_hidden_attr.cursor = 1;
    add_event_listener(input, EVENT_KEYBOARD_ONLY, &EMBEDDED_input_input_handler);
    add_event_listener(input, EVENT_BLUR, &EMBEDDED_input_blur_handler);
    add_event_listener(input, EVENT_FOCUS, &EMBEDDED_input_focus_handler);
    return input;
}

void focus(document *doc) {
    blur(doc->ctx->focused);
    document_focus(doc);
    dispatch_event(create_event_of_type(doc, EVENT_FOCUS));
}

void blur(document *doc) {
    document_unfocus(doc);
    dispatch_event(create_event_of_type(doc, EVENT_BLUR));
}

position get_cursor_position(document *doc) {
    int bx, by;
    getbegyx(doc->win, by, bx);
    position p;
    p.x = bx + doc->_hidden_attr.cur_pos.x;
    p.y = by + doc->_hidden_attr.cur_pos.y;
    return p;
}

void document_set_text(document *doc, const wchar_t *text) {
    if (doc->text) free(doc->text);
    doc->text = wclonestr(text);
    document_request_update(doc);
}
