#include "dom.h"

document *create_document() {
    document *doc = (document *) malloc(sizeof(document));
    doc->ctx = NULL;
    doc->type = COMPONENT_TYPE_DIV;
    doc->name = NULL;
    doc->key = time(NULL);
    doc->show = 1;
    doc->should_update = 1;
    doc->should_hard_update = 1;
    doc->children = NULL;
    doc->text = NULL;
    doc->ch_len = 0;
    doc->parent = NULL;
    doc->state = NULL;
    doc->styles = NULL;
    doc->event_handlers = create_dict();
    doc->win = NULL;
    doc->_hidden_attr.value = NULL;
    doc->_hidden_attr.extra_win = NULL;
    doc->_hidden_attr.cur_pos.x = 0;
    doc->_hidden_attr.cur_pos.y = 0;
    doc->_hidden_attr.cursor = 0;
    return doc;
}

void destroy_document(document *doc) {
    if (!doc) return;
    if (doc->name) free(doc->name);
    if (doc->text) free(doc->text);
    if (doc->ch_len && doc->children) {
        for (size_t i = 0; i < doc->ch_len; ++i)
            if (doc->children[i]) destroy_document(doc->children[i]);
        free(doc->children);
    }
    if (doc->event_handlers) dict_destroy(doc->event_handlers, (cleaner_t) (&destroy_event_handler_set));
    if (doc->styles) dict_destroy(doc->styles, (cleaner_t) (NULL)); // destroy styles
    if (doc->state) dict_destroy(doc->event_handlers, (cleaner_t) (NULL)); // destroy state
    if (doc->_hidden_attr.extra_win) delwin(doc->_hidden_attr.extra_win);
    if (doc->_hidden_attr.value) free(doc->_hidden_attr.value);
    if (doc->win) delwin(doc->win);
    free(doc);
}

void document_append_child(document *parent, document *child) {
    document **new_ch = (document **) malloc(sizeof(document *) * ++parent->ch_len);
    new_ch[parent->ch_len - 1] = child;
    if (!(parent->ch_len - 1))
        parent->children = new_ch;
    else {
        for (size_t i = 0; i < parent->ch_len - 1; ++i)
            new_ch[i] = parent->children[i];
        free(parent->children);
        parent->children = new_ch;
    }
    parent->should_update = 1;
    child->parent = parent;
    child->ctx = parent->ctx;
}

document *document_get_root(document *doc) {
    while (doc->parent) doc = doc->parent;
    return doc;
}

document *get_focused_document(document *target) {
    return target->ctx->focused;
}

void document_unfocus(document *doc) {
    doc->ctx->focused = NULL;
    doc->should_update = 1;
}

void document_focus(document *doc) {
    doc->ctx->focused = doc;
    doc->ctx->focused->should_update = 1;
}

document *get_next_focus(document *doc, int ignore_children) {
    if (!ignore_children && doc->ch_len) return doc->children[0];
    document *p = doc->parent;
    if (!p) return doc;
    size_t idx;
    for (idx = 0; idx < p->ch_len; ++idx)
        if (p->children[idx] == doc)
            break;
    if (idx == p->ch_len - 1) return get_next_focus(p, 1);
    return p->children[idx + 1];
}

document *get_prev_focus(document *doc, int ignore_parent) {
    document *p = doc->parent;
    if (!p || ignore_parent)
        return doc->ch_len ? get_prev_focus(doc->children[doc->ch_len - 1], 1) : doc;
    size_t idx;
    for (idx = 0; idx < p->ch_len; ++idx)
        if (p->children[idx] == doc)
            break;
    if (idx == 0) return p;
    else return p->children[idx - 1];
}

void document_request_update(document *doc) {
    doc->should_update = 1;
}

void document_request_hard_update(document *doc) {
    doc->should_update = 1;
    doc->should_hard_update = 1;
}
