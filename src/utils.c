#include "utils.h"

void null_free(void **x) {
    free(*x);
    *x = NULL;
}

char *clonestr(const char *src) {
    size_t len = strlen(src);
    char *dst = (char *) malloc(sizeof(char) * (len + 1));
    strcpy(dst, src);
    return dst;
}

wchar_t *wclonestr(const wchar_t *src) {
    size_t len = wcslen(src);
    wchar_t *dst = (wchar_t *) malloc(sizeof(wchar_t) * (len + 1));
    wcscpy(dst, src);
    return dst;
}

wchar_t *newwstr(size_t len) {
    wchar_t *newstr = (wchar_t *) malloc(sizeof(wchar_t) * (len + 1));
    for (size_t i = 0; i <= len; ++i)
        newstr[i] = 0;
    return newstr;
}

wchar_t char2wchar(char src) {
    wchar_t dst;
    mbstowcs(&dst, &src, 1);
    return dst;
}

wchar_t *chars2wchars(const char *src) {
    size_t len = strlen(src);
    wchar_t *dst = (wchar_t *) malloc(sizeof(wchar_t) * (len + 1));
    mbstowcs(dst, src, len + 1);
    return dst;
}

void remove_by_idx(void ***arr, size_t size, size_t idx, cleaner_t cleaner) {
    void **new_arr = (void **) malloc(sizeof(void *) * size - 1);
    for (size_t i = 0, j = 0; i < size; ++i)
        if (i != idx) new_arr[j++] = (*arr)[i];
        else if (cleaner) cleaner((*arr)[i]);
    free(*arr);
    *arr = new_arr;
}

bool is_printable(unsigned long c) {
    return !is_remove_char(c) && !is_caret_move(c) && c != 353 && iswprint(c);
}

bool is_caret_move(unsigned long c) {
    return c >= 258 && c <= 261;
}

bool is_backspace(unsigned long c) {
    return c == 263 || c == 8;
}

bool is_remove_char(unsigned long c) {
    return is_backspace(c) || c == 330;
}

void win_clean(WINDOW *win) {
    int maxx, maxy;
    getmaxyx(win, maxy, maxx);
    for (int i = 0; i < maxy; ++i)
        for (int j = 0; j < maxx; ++j)
            mvwprintw(win, i, j, "%C", ' ');
}
