#include "events.h"

static set *event_queue;

void init_events() {
    event_queue = create_set();
}

void destroy_events() {
    destroy_set(event_queue, (cleaner_t) &destroy_native_event);
}

event_handlers_set create_event_handlers_set() {
    event_handlers_set ehs = malloc(sizeof(struct evt_handlers_s));
    ehs->len = 0;
    ehs->cap = 10;
    ehs->handlers = (evt_handler *) malloc(sizeof(evt_handler) * ehs->cap);
    return ehs;
}

void event_stop_propagation(native_event *e) {
    e->is_stopped = 1;
}

void destroy_event_handler_set(event_handlers_set ehs) {
    free(ehs->handlers);
    free(ehs);
}

native_event *create_native_event() {
    native_event *e = (native_event *) malloc(sizeof(native_event));
    e->code = -1;
    e->mouse_event = NULL;
    e->is_stopped = 0;
    e->target = NULL;
    e->cd_len = 0;
    e->codes = malloc(sizeof(int) * 64);
    e->type = NULL;
    for (int i = 0; i < 64; ++i) e->codes[i] = 0;
    return e;
}

native_event *create_event_of_type(document *target, const char *type) {
    native_event *e = create_native_event();
    e->type = (char *) type;
    e->target = target;
    return e;
}

void destroy_native_event(native_event *e) {
    if (!e) return;
    if (e->mouse_event)
        free(e->mouse_event);
    free(e->codes);
    free(e);
}

context *create_event_context(native_event *e, cui_window global_ctx) {
    context *ctx = (context *) malloc(sizeof(context));
    ctx->emitter = NULL;
    ctx->event = e;
    ctx->app = global_ctx;
    ctx->store = global_ctx->store;
    return ctx;
}

void destroy_event_context(context *ctx) {
    free(ctx);
}

void add_event_handler(event_handlers_set ehs, evt_handler cb) {
    for (ssize_t i = 0; i < ehs->len; ++i)
        if (ehs->handlers[i] == cb) return;
    ehs->handlers[ehs->len] = cb;
    ehs->len++;
    if (ehs->len == ehs->cap) {
        ehs->cap = ehs->cap * 2;
        evt_handler *new_handlers = (evt_handler *) malloc(sizeof(evt_handler) * ehs->cap);
        for (int i = 0; i < ehs->len; ++i)
            new_handlers[i] = ehs->handlers[i];
        free(ehs->handlers);
        ehs->handlers = new_handlers;
    }
}

void remove_event_handler(event_handlers_set ehs, evt_handler cb) {
    ssize_t idx = -1;
    for (; idx < ehs->len; ++idx)
        if (ehs->handlers[idx] == cb) break;
    if (idx == -1) return;
    remove_by_idx((void ***) &ehs->handlers, ehs->len-- + 1, idx, NULL);
}

native_event *generate_native_event(cui_window _ctx) {
    document *focused = _ctx->focused;
    position cur_pos = get_cursor_position(focused);
    unsigned int code;
    int ret = mvget_wch(cur_pos.y, cur_pos.x, &code);
    if (ret == ERR) return NULL;
    native_event *e = create_native_event();
    e->target = _ctx->focused;
    e->code = code;
    nodelay(stdscr, TRUE);
    e->codes[0] = e->code;
    e->cd_len = 1;
    while (mvget_wch(cur_pos.y, cur_pos.x, e->codes + e->cd_len) != ERR) e->cd_len++;
    if (e->code == KEY_MOUSE) {
        e->mouse_event = (MEVENT *) malloc(sizeof(MEVENT));
        getmouse(e->mouse_event);
    }
    e->type = e->mouse_event ? EVENT_MOUSE_ONLY : EVENT_KEYBOARD_ONLY;
    nodelay(stdscr, FALSE);
    timeout(EVT_TIMEOUT);
    return e;
}

void call_event_emitter(evt_handler cb, native_event *e, cui_window global_ctx, document *emd) {
    context *ctx = create_event_context(e, global_ctx);
    ctx->emitter = emd;
    cb(ctx);
    destroy_event_context(ctx);
}

void resolve_ehs(event_handlers_set ehs, native_event *e, cui_window global_ctx, document *emd) {
    if (!ehs) return;
    for (ssize_t i = 0; i < ehs->len; ++i)
        call_event_emitter(ehs->handlers[i], e, global_ctx, emd);
}

void dispatch_event(native_event *e) {
    set_push(event_queue, e);
}

void resolve_events(cui_window global_ctx) {
    native_event *e;
    while ((e = set_pop(event_queue))) {
        document *emd = e->target;
        if (!emd) {
            document *root = global_ctx->dom;
            resolve_ehs((event_handlers_set) dict_get(root->event_handlers, e->type), e, global_ctx, NULL);
            resolve_ehs((event_handlers_set) dict_get(root->event_handlers, EVENT_ALL), e, global_ctx, NULL);
        }
        while (emd && !e->is_stopped) {
            resolve_ehs((event_handlers_set) dict_get(emd->event_handlers, e->type), e, global_ctx, emd);
            resolve_ehs((event_handlers_set) dict_get(emd->event_handlers, EVENT_ALL), e, global_ctx, emd);
            emd = emd->parent;
        }
        destroy_native_event(e);
    }
}

void process_events(cui_window _ctx) {
    native_event *e = generate_native_event(_ctx);
    if (e) set_push(event_queue, e);
    resolve_events(_ctx);
}

