#include "cuil.h"

border_preset BORDER_PRESETS[] = {
        {L' ', L' ', L' ', L' ', L' ', L' ', L' ', L' '},
        {L'▛', L'▜', L'▙', L'▟', L'▌', L'▐', L'▀', L'▄'},
        {L'▛', L'▜', L'▙', L'▟', L' ', L' ', L' ', L' '},
        {L'┌', L'┐', L'└', L'┘', L'│', L'│', L'─', L'─'},
        {L'┌', L'┐', L'└', L'┘', L' ', L' ', L' ', L' '},
};

void draw_border_preset(WINDOW *win, size_t preset) {
    int maxy, maxx;
    getmaxyx(win, maxy, maxx);
    maxy--;
    maxx--;
    border_preset b = BORDER_PRESETS[preset];
    mvwprintw(win, 0, 0, "%C", b.lt);
    mvwprintw(win, 0, maxx, "%C", b.rt);
    mvwprintw(win, maxy, 0, "%C", b.lb);
    mvwprintw(win, maxy, maxx, "%C", b.rb);
    for (int i = 1; i < maxx; ++i) {
        mvwprintw(win, 0, i, "%C", b.t);
        mvwprintw(win, maxy, i, "%C", b.b);
    }
    for (int i = 1; i < maxy; ++i) {
        mvwprintw(win, i, 0, "%C", b.l);
        mvwprintw(win, i, maxx, "%C", b.r);
    }
}

void draw_border_char(WINDOW *win, wchar_t c, short bg, short fg, short decoration) {
    int maxy, maxx;
    getmaxyx(win, maxy, maxx);
    maxy--;
    maxx--;
    if (has_colors() == FALSE) {
        for (int i = 0; i <= maxx; ++i) {
            mvwprintw(win, 0, i, "%C", c);
            mvwprintw(win, maxy, i, "%C", c);
        }
        for (int i = 1; i < maxy; ++i) {
            mvwprintw(win, i, 0, "%C", c);
            mvwprintw(win, i, maxx, "%C", c);
        }
        return;
    }

    bg = fg = decoration = 0;
}

void draw_input(document *input) {
    if (IS_FOCUSED(input)) draw_border_preset(input->win, BORDER_THIN);
    else
        draw_border_preset(input->win, BORDER_THIN_CORNERS);
    mvwprintw(input->win, 0, 0, "%S", input->_hidden_attr.value);
    wrefresh(input->win);
}

void draw_div(document *div) {
    win_clean(div->win);
    if (IS_FOCUSED(div)) draw_border_preset(div->win, BORDER_THIN);
    else
        draw_border_preset(div->win, BORDER_THIN_CORNERS);
    if (div->text)
        mvwprintw(div->win, 0, 0, "%S", div->text);
    wrefresh(div->win);
}

void draw_frame(document *doc, int force) {
    if (doc->should_update || force) {
        (doc->type == COMPONENT_TYPE_INPUT) ? draw_input(doc) : draw_div(doc);
        doc->should_update = 0;
        doc->should_hard_update = 0;
        force = 1;
    }
    if (doc->ch_len)
        for (size_t i = 0; i < doc->ch_len; ++i)
            if (doc->children[i]->show)
                draw_frame(doc->children[i], force);
}

void set_cursor(cui_window app) {
    document *focused = app->focused;
    wmove(focused->win, focused->_hidden_attr.cur_pos.y, focused->_hidden_attr.cur_pos.x);
    curs_set(focused->_hidden_attr.cursor);
}
