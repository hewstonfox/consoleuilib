#include "queue.h"

queue *create_queue() {
    queue *q = (queue *) malloc(sizeof(queue));
    q->cap = 10;
    q->len = 0;
    q->data = (void **) malloc(sizeof(void *) * q->cap);
    return q;
}

bool queue_includes(queue *q, void *data) {
    for (int i = 0; i < q->len; ++i)
        if (q->data[i] == data) return true;
    return false;
}

void queue_push(queue *q, void *data) {
    q->data[q->len++] = data;
    if (q->len == q->cap) {
        q->cap = q->cap * 2;
        void **new_data = (void **) malloc(sizeof(void *) * q->cap);
        for (int i = 0; i < q->len; ++i)
            new_data[i] = q->data[i];
        free(q->data);
        q->data = new_data;
    }
}

void *queue_pop(queue *q) {
    if (!q->len) return NULL;
    void *data = q->data[0];
    for (int i = 1; i < q->len; ++i)
        q->data[i - 1] = q->data[i];
    q->len--;
    return data;
}

void destroy_queue(queue *q, cleaner_t cleaner) {
    if (cleaner) for (int i = 0; i < q->len; ++i) cleaner(q->data[i]);
    free(q->data);
    free(q);
}
