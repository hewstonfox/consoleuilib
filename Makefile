.OBJECTS = $(wildcard src/*.c)
OBJECTS = $(.OBJECTS:src%.c=obj%.o)
CC=clang
CFLAGS=-std=c11 -Wall -Wextra -Werror -Wpedantic -g -Wno-error=unused-command-line-argument
NAME=cuil

all: prepare $(NAME)

prepare:
	mkdir -p obj/

$(NAME): $(OBJECTS)
	ar -rcs $(NAME).a $^

obj/%.o: src/%.c
	$(CC) $(CFLAGS) -Iinc -o $@ -c $<

clean:
	rm -rf obj

uninstall: clean
	rm -f $(NAME).a

reinstall: uninstall all

run: prepare $(NAME) $(OBJECTS)
	$(CC) $(CFLAGS) -o main __main.c cuil.a -lncursesw -lpthread

